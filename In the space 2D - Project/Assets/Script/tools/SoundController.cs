﻿using UnityEngine;

namespace Juego.Tools
{
    public class SoundController : MonoBehaviour
    {
        public static SoundController instance = null;

        #region referencias en unity

        [Header("Zona Tracks")]

        [SerializeField] private AudioSource tracks_Source = null;
        [SerializeField] private AudioClip[] tracks = null;


        [Header("Zona Sfx")]
        [SerializeField] private AudioSource[] sfxSource = null;


        [SerializeField] private AudioClip[] sfxDisparos = null;

        [SerializeField] private AudioClip[] sfxReciveDmg = null;

        [SerializeField] private AudioClip[] sfxEnemiesReciveDmg = null;

        [SerializeField] private AudioClip[] sfxBtnMenu = null;
        #endregion

        #region unity calls
        private void Awake()
        {
            Initialize();
        }

        private void Start()
        {
            SetTrack(tracks[0], 0.2f);
        }
        #endregion


        #region public methods

        public void PlaySfxDisparos(int index, float volume = 1.0f, float pitch = 1.0f, float pan = 1.0f)
        {
            sfxSource[0].volume = volume;
            sfxSource[0].pitch = pitch;
            sfxSource[0].panStereo = pan;
            sfxSource[0].PlayOneShot(sfxDisparos[index]);
        }

        public void PlaySfxReciveDmg(int index, float volume, float pitch, float pan)
        {
            sfxSource[1].volume = volume;
            sfxSource[1].pitch = pitch;
            sfxSource[1].PlayOneShot(sfxReciveDmg[index]);
        }

        public void PlaySfxEnemiesReciveDmg(int index, float volume, float pitch, float pan)
        {
            sfxSource[2].volume = volume;
            sfxSource[2].pitch = pitch;
            sfxSource[2].panStereo = pan;
            sfxSource[2].PlayOneShot(sfxEnemiesReciveDmg[index]);
        }

        public void PlaySfxBtnMenu(int index, float volume = 0.1f, float pitch = 1.0f)
        {
            sfxSource[3].volume = volume;
            sfxSource[3].pitch = pitch;
            sfxSource[3].PlayOneShot(sfxBtnMenu[index]);
        }

        public void PauseTrack()
        {
            tracks_Source.Pause();
        }

        public void SubirVolume()
        {
            tracks_Source.volume += 0.1f;
        }

        public void BajarVolume()
        {
            tracks_Source.volume -= 0.1f;
        }

        public float GetVolume()
        {
            return tracks_Source.volume * 100.0f;
        }

        #endregion

        #region private methods
        private void Initialize()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void SetTrack(AudioClip tracks, float volume)
        {
            tracks_Source.clip = tracks;
            tracks_Source.volume = volume;
            tracks_Source.Play();
            tracks_Source.loop = true;

        }

        #endregion

    }
}
