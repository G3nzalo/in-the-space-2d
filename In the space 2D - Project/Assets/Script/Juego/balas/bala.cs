﻿using UnityEngine;

namespace Juego.Balas
{
    public class bala : MonoBehaviour
    {
        [SerializeField] private float speedBala = 10.0f;

        private Rigidbody2D rigiBody = null;


        private void Awake()
        {
            rigiBody = GetComponent<Rigidbody2D>();
        }

        private void Start()
        {
            rigiBody.isKinematic = false;
            rigiBody.AddForce(transform.up * speedBala * Time.deltaTime, ForceMode2D.Impulse);
        }

        private void OnTriggerEnter2D(Collider2D otro)
        {
            if (otro.gameObject.CompareTag("Bala"))
            {
                Destroy(otro.gameObject);
                Destroy(gameObject);
            }
            if (otro.gameObject.CompareTag("BalaEnemigo1"))
            {
                Destroy(otro.gameObject);
                Destroy(gameObject);
            }
            if (otro.gameObject.CompareTag("BalaEnemigo2"))
            {
                Destroy(otro.gameObject);
                Destroy(gameObject);
            }
            if (otro.gameObject.CompareTag("BalaEnemigo3"))
            {
                Destroy(otro.gameObject);
                Destroy(gameObject);
            }
        }

    }
}
