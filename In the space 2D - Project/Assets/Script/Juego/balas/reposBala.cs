﻿using UnityEngine;

namespace Juego.Balas
{
    public class reposBala : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D otro)
        {
            if (otro.gameObject.CompareTag("Bala"))
            {
                Destroy(otro.gameObject);
            }
        }
    }
}