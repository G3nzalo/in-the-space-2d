﻿using UnityEngine;

namespace Juego.Balas
{
    public class autoDestruir : MonoBehaviour
    {
        [SerializeField] private float timer = 2.0f;

        void Update()
        {
            Destroy(gameObject, timer);
        }
    }
}
