﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Juego.Tools;

namespace Juego.Ui
{
    public class credits : MonoBehaviour
    {
        public static credits instance = null;

        private void Awake()
        {
            Initialize();
        }

        public void VolverMenuPrincipal()
        {
            SoundController.instance.PlaySfxBtnMenu(0);
            SoundController.instance.PauseTrack();
            SceneManager.LoadScene(0);
        }

        public void SalirDelJuego()
        {
            SoundController.instance.PlaySfxBtnMenu(0);
            Application.Quit();
        }

        private void Initialize()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
