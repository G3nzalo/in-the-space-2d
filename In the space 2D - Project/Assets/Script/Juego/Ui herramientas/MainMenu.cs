﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using Juego.Tools;

namespace Juego.Ui
{
    public class MainMenu : MonoBehaviour
    {
        #region unity referencias
        [Header("Referencias")]
        [SerializeField] private GameObject panelInstrucciones = null;
        [SerializeField] private GameObject btnMenu = null;
        [SerializeField] private GameObject canvas = null;
        #endregion


        public static MainMenu instance = null;

        #region unity Calls
        private void Awake()
        {
            Initialize();
        }

        private void Update()
        {
            ChekInput();
        }
        #endregion

        #region public methods

        public void ChekInput()
        {
            if (Input.GetKey(KeyCode.Return))
            {
                SoundController.instance.PlaySfxBtnMenu(0);
                panelInstrucciones.SetActive(true);
                btnMenu.SetActive(false);

            }

            if (Input.GetKey(KeyCode.Space))
            {
                SoundController.instance.PlaySfxBtnMenu(0);
                StartCoroutine(Load());
            }
        }

        public void CargarPanelInstrucciones()
        {
            SoundController.instance.PlaySfxBtnMenu(0);
            panelInstrucciones.SetActive(true);
            btnMenu.SetActive(false);
        }

        public void CargarNivel1()
        {
            SoundController.instance.PlaySfxBtnMenu(0);
            StartCoroutine(Load());
        }

        #endregion

        #region private methods
        private void Initialize()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

        }

        private IEnumerator Load()
        {
            Destroy(canvas);
            AsyncOperation async = SceneManager.LoadSceneAsync(1);
            async.allowSceneActivation = true;

            while (!async.isDone)
            {
                yield return null;
            }

        }

        #endregion

    }
}

