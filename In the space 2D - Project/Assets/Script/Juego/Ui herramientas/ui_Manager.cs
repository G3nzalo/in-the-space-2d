﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using Juego.Tools;

namespace Juego.Ui
{
    public class ui_Manager : MonoBehaviour
    {
        #region ref en unity
        [SerializeField] private int numEnemiesIinit = 0;

        [Header("Barra Vida Nave")]
        [SerializeField] public BarraVida[] barravidaNave = null;
        #endregion

        public static ui_Manager instance = null;

        #region public vars
        public GameObject optionsPanel = null;
        public GameObject ajustesPanel = null;
        public GameObject atackPanel = null;
        public TextMeshProUGUI numEnemies = null;
        public TextMeshProUGUI volumeActual = null;
        #endregion

        #region unity calls
        private void Awake()
        {
            Initialize();
            numEnemies.text = ((int)numEnemiesIinit).ToString();
            gameObject.SetActive(true);
        }

        private void Update()
        {
            volumeActual.text = SoundController.instance.GetVolume().ToString();
        }
        #endregion

        #region public methods
        public void OptionsPanel()
        {
            Time.timeScale = 0;
            optionsPanel.SetActive(true);
            ajustesPanel.SetActive(false);
            SoundController.instance.PlaySfxBtnMenu(0);
        }

        public void Return()
        {
            Time.timeScale = 1;
            optionsPanel.SetActive(false);
            ajustesPanel.SetActive(false);
            SoundController.instance.PlaySfxBtnMenu(0);
        }

        public void OtrasOpciones()
        {
            optionsPanel.SetActive(false);
            ajustesPanel.SetActive(true);
            SoundController.instance.PlaySfxBtnMenu(0);
        }

        public void VolverMenuPrincipal()
        {
            Time.timeScale = 1;
            SoundController.instance.PlaySfxBtnMenu(0);
            optionsPanel.SetActive(false);
            ajustesPanel.SetActive(false);
            gameObject.SetActive(false);
            SoundController.instance.PauseTrack();
            SceneManager.LoadScene(0);

        }

        public void SalirDelJuego()
        {
            SoundController.instance.PlaySfxBtnMenu(0);
            Application.Quit();

        }

        public void SubirMusic()
        {
            SoundController.instance.PlaySfxBtnMenu(0);
            SoundController.instance.SubirVolume();
        }

        public void BajarMusic()
        {
            SoundController.instance.PlaySfxBtnMenu(0);
            SoundController.instance.BajarVolume();
        }

        public void SetCurNumEnemies(int enemies)
        {
            numEnemiesIinit -= enemies;
            numEnemies.text = numEnemiesIinit.ToString();

            if (numEnemiesIinit == 0)
            {
                SceneManager.LoadScene(3);
            }

        }
        #endregion

        private void Initialize()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

        }

    }
}
