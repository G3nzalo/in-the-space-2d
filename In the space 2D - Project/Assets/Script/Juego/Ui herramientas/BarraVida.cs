﻿using UnityEngine;
using UnityEngine.UI;

namespace Juego.Ui
{
    public class BarraVida : MonoBehaviour
    {
        #region Referencias y Asignaciones en Unity
        [Header("Referencias & Atributos")]
        [SerializeField] private Image fill = null;
        [SerializeField] private Color[] colorRanges = null;
        [SerializeField] private float lerpTime = 1.0f;
        [SerializeField] private AbstractLerper<Color>.SMOOTH_TYPE colorSmooth = AbstractLerper<Color>.SMOOTH_TYPE.EXPONENTIAL;
        [SerializeField] private AbstractLerper<float>.SMOOTH_TYPE fillSmooth = AbstractLerper<float>.SMOOTH_TYPE.EXPONENTIAL;
        #endregion

        #region private vars
        private ColorLerper barraLerper = null;
        private FloatLerper fillLerper = null;
        private float maxHp = 0.0f;
        #endregion

        #region Unity Calls
        private void Awake()
        {
            barraLerper = new ColorLerper(lerpTime, colorSmooth);
            fillLerper = new FloatLerper(lerpTime, fillSmooth);
        }

        void Update()
        {
            //si esta prendida 
            if (barraLerper.On)
            {
                barraLerper.Update();
                fill.color = barraLerper.CurrentValue;
            }

            if (fillLerper.On)
            {
                fillLerper.Update();
                fill.fillAmount = fillLerper.CurrentValue;
            }
        }
        #endregion

        #region Public methods
        public void ConfigureBar(float hp, float maxHp)
        {
            this.maxHp = maxHp;
            UpdateHpBar(hp);
        }

        public void UpdateHpBar(float hp)
        {
            fillLerper.SetValues(fill.fillAmount, hp / maxHp, true);
            barraLerper.SetValues(fill.color, GetColor(hp / maxHp), true);
        }
        #endregion

        #region Private Metods
        private Color GetColor(float hp)
        {
            return Color.Lerp(colorRanges[0], colorRanges[1], hp);
        }
        #endregion
    }
}

