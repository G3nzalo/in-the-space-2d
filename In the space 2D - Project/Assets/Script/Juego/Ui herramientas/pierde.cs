﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Juego.Tools;

namespace Juego.Ui
{
    public class pierde : MonoBehaviour
    {
        public static pierde instance = null;

        private void Awake()
        {
            Initialize();

        }

        #region public methods
        public void ReloadGame()
        {
            SoundController.instance.PlaySfxBtnMenu(0);
            SoundController.instance.PauseTrack();
            SceneManager.LoadScene(1);
        }

        public void VolverMenuPrincipal()
        {
            SoundController.instance.PlaySfxBtnMenu(0);
            SoundController.instance.PauseTrack();
            SceneManager.LoadScene(0);
        }

        public void SalirDelJuego()
        {
            SoundController.instance.PlaySfxBtnMenu(0);
            Application.Quit();
        }
        #endregion

        private void Initialize()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
