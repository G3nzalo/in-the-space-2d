﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Juego.Ui;
using Juego.Tools;

namespace Juego.Entidades
{
    public class controller_nave : MonoBehaviour
    {
        #region Referencias en Unity
        [Header("Referencias a los objetos")]
        [SerializeField] private GameObject prefabBala = null;
        [SerializeField] private Transform lugarCreaBala = null;
        [SerializeField] private GameObject fuego = null;

        [Header("Ref scriptObj Nave")]
        [SerializeField] private naveConfiguration naveConfiguracion = null;

        [Header("Referencias a Enemigos")]
        [SerializeField] private controller_enemigos[] enemigos = null;
        #endregion

        #region vars privadas
        private Rigidbody2D rigiBody = null;

        private Vector2 movDiff = Vector2.zero;

        private Quaternion rotDiff = Quaternion.Euler(Vector3.zero);

        private float horizontal = 0.0f;
        private float vertical = 0.0f;
        private float rotacion = 0.0f;
        private float curTimeDis = 0;
        private float vida = 0.0f;
        #endregion

        #region private Unity methods 
        private void Awake()
        {
            rigiBody = GetComponent<Rigidbody2D>();
            vida = naveConfiguracion.startingHP;
            curTimeDis = naveConfiguracion.tiempoCreaDisparo;
        }

        private void Start()
        {
            ui_Manager.instance.barravidaNave[0].ConfigureBar(vida, vida);
            ui_Manager.instance.barravidaNave[1].ConfigureBar(vida, vida);
        }

        void Update()
        {
            ReadAxisValues();
            RotateNave(rotacion);
            CheckDisparo();
            CheckPause();
            CheckAxisFuego(horizontal, vertical, rotacion);
        }

        private void FixedUpdate()
        {
            MoverNave(vertical, horizontal);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("BalaEnemigo1"))
            {
                SacaVida(enemigos[0].GetDmg());
                SoundController.instance.PlaySfxReciveDmg(Random.Range(0, 3), Random.Range(0.02f, 0.05f), Random.Range(0.6f, 1.8f), transform.position.x);
                Destroy(collision.gameObject);

            }

            if (collision.gameObject.CompareTag("BalaEnemigo2"))
            {
                SacaVida(enemigos[1].GetDmg());
                SoundController.instance.PlaySfxReciveDmg(Random.Range(0, 3), Random.Range(0.02f, 0.05f), Random.Range(0.6f, 1.8f), transform.position.x);
                Destroy(collision.gameObject);

            }

            if (collision.gameObject.CompareTag("BalaEnemigo3"))
            {
                SacaVida(enemigos[2].GetDmg());
                SoundController.instance.PlaySfxReciveDmg(Random.Range(0, 3), Random.Range(0.02f, 0.05f), Random.Range(0.6f, 1.8f), transform.position.x);
                Destroy(collision.gameObject);
            }

        }
        #endregion

        #region public methds
        public float GetDmg()
        {
            return naveConfiguracion.dmg;
        }
        #endregion

        #region privates methods Nave

        private void ReadAxisValues()
        {
            horizontal = Input.GetAxis(naveConfiguracion.horizontal);
            vertical = Input.GetAxis(naveConfiguracion.vertical);
            rotacion = Input.GetAxis(naveConfiguracion.rotacion);
        }

        private void MoveNaveVertical(float axis)
        {
            rigiBody.velocity = transform.up * axis * naveConfiguracion.speed * Time.deltaTime;

        }

        private void MoveNaveHorizontal(float axis)
        {
            rigiBody.velocity = transform.right * axis * naveConfiguracion.speed * Time.deltaTime;

        }

        private void MoverNave(float axisV, float axisH)
        {
            if (axisV != 0.0f)
            {
                MoveNaveVertical(axisV);
            }
            else
            {
                MoveNaveHorizontal(axisH);
            }

        }

        private void RotateNave(float axis)
        {
            rotDiff.z = axis * naveConfiguracion.rotationSpeed * Time.deltaTime;
            rigiBody.MoveRotation(rotDiff * transform.localRotation);
        }

        private void CheckDisparo()
        {
            if (Input.GetButton(naveConfiguracion.fire))
            {
                curTimeDis -= 1.0f;

                if (curTimeDis <= 0.0f)
                {
                    Disparo();
                    SoundController.instance.PlaySfxDisparos(Random.Range((int)0.0f, (int)3.0f), Random.Range(0.1f, 0.2f), Random.Range(1.0f, 2.5f), transform.position.x);
                    curTimeDis = naveConfiguracion.tiempoCreaDisparo;
                }
            }
        }

        private void Disparo()
        {
            GameObject bala = Instantiate(prefabBala);
            bala.transform.position = lugarCreaBala.position;
            bala.transform.rotation = transform.rotation;
        }

        private void SacaVida(float dmg)
        {
            vida -= dmg;

            if (vida > 0.0f)
            {
                ui_Manager.instance.barravidaNave[0].UpdateHpBar(vida);
                ui_Manager.instance.barravidaNave[1].UpdateHpBar(vida);
            }

            if (vida <= 0)
            {
                SceneManager.LoadScene(2);
            }
        }

        private void CheckPause()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ui_Manager.instance.OptionsPanel();
            }
        }

        private void CheckAxisFuego(float axis1, float axis2, float axis3)
        {
            if (axis1 != 0 || axis2 != 0 || axis3 != 0)
            {
                fuego.SetActive(true);
            }
            else { fuego.SetActive(false); }
        }
        #endregion

    }
}
