﻿using UnityEngine;

[CreateAssetMenu(fileName = "NaveConfiguration", menuName = "NaveConfiguration", order = 0)]

public class naveConfiguration : ScriptableObject
{
    [Header("Seccion Controles")]
    public string horizontal;
    public string vertical;
    public string rotacion;
    public string fire;

    [Header("Seccion Variables")]
    public float startingHP;
    public float speed;
    public float rotationSpeed;
    public float dmg;
    public float tiempoCreaDisparo;

}