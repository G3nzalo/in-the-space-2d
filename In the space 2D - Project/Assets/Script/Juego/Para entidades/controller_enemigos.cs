﻿using UnityEngine;
using Juego.Tools;
using Juego.Ui;

namespace Juego.Entidades
{
    public class controller_enemigos : MonoBehaviour
    {
        #region refrencias en Unity
        [Header("Referencias a los objetos")]
        [SerializeField] private GameObject prefabBala = null;
        [SerializeField] private Transform lugarCreaBala = null;
        [SerializeField] private Transform lugarCreaBala2 = null;
        #endregion

        #region Vars en unity
        [Header("Vars")]
        [SerializeField] private float timerCrearBala = 0.0f;
        [SerializeField] private float speedMov = 2.0f;
        [SerializeField] private float radioVision = 2.0f;
        [SerializeField] private float vida = 400.0f;
        [SerializeField] private float dmg = 10.0f;
        #endregion

        #region private Vars
        private GameObject player;
        private Rigidbody2D rigiBody = null;

        private Vector2 initialPos;
        private Vector2 target;

        private controller_nave nave = null;

        private float dist = 0.0f;
        private float timerCurr = 0.0f;
        #endregion

        #region Unity calls
        private void Awake()
        {
            rigiBody = GetComponent<Rigidbody2D>();
            nave = FindObjectOfType<controller_nave>();
            timerCurr = timerCrearBala;
        }

        private void Start()
        {
            player = GameObject.FindGameObjectWithTag("Nave");
            initialPos = transform.position;
        }

        void Update()
        {
            target = initialPos;
            CalculoDistancia();
            CheckDistancia();

            Debug.DrawLine(transform.position, target, Color.red);
            timerCrearBala--;
            CheckDisparo();

            RotateNave();
        }


        private void FixedUpdate()
        {
            MoverNave();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Bala"))
            {
                SacaVida(nave.GetDmg());
                Destroy(collision.gameObject);
            }
        }

        #endregion

        #region public Methods
        public float GetDmg()
        {
            return dmg;
        }
        #endregion

        #region private methods
        private void CheckDisparo()
        {
            if (Time.timeScale != 0)
            {
                if (timerCrearBala <= 0.0f)
                {
                    Disparo();
                    timerCrearBala = timerCurr;
                }
            }
        }

        private void Disparo()
        {
            GameObject bala1 = Instantiate(prefabBala);
            bala1.transform.position = lugarCreaBala.position;
            bala1.transform.rotation = transform.rotation;

            GameObject bala2 = Instantiate(prefabBala);
            bala2.transform.position = lugarCreaBala2.position;
            bala2.transform.rotation = transform.rotation;
        }

        private void CheckDistancia()
        {
            if (dist < radioVision)
            {
                if (dist > 5.0f)
                {
                    target = player.transform.position;
                }
            }
        }

        private void CalculoDistancia()
        {
            dist = Vector2.Distance(player.transform.position, transform.position);
        }

        private void MoverNave()
        {
            float speed = speedMov * Time.deltaTime;
            rigiBody.MovePosition(Vector2.MoveTowards(transform.position, target, speed));

        }

        private void RotateNave()
        {
            if (player.transform.position == null)
            {
                return;
            }
            else
            {
                CalculoDeFisicaParaRotar();
            }
        }

        private void SacaVida(float dmg)
        {
            vida -= dmg;

            if (vida > 0.0f)
            {
                SoundController.instance.PlaySfxEnemiesReciveDmg(0, Random.Range(0.1f, 0.2f), Random.Range(0.4f, 0.8f), transform.position.x);
            }

            if (vida <= 0.0f)
            {
                Destroy(gameObject);
                ui_Manager.instance.SetCurNumEnemies(1);
            }
        }

        private void CalculoDeFisicaParaRotar()
        {
            Vector3 vectorToTarget = player.transform.position - transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            q *= Quaternion.Euler(0.0f, 0.0f, -90.0f);
            rigiBody.MoveRotation(q);
        }
        #endregion
    }
}
