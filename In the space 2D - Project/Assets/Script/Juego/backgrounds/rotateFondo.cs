﻿using UnityEngine;

namespace Juego.Backgrounds
{
    public class rotateFondo : MonoBehaviour
    {
        [SerializeField] private float speedRot = 0.0f;

        #region vars private
        private Quaternion rotDiff = Quaternion.Euler(Vector3.zero);
        private Rigidbody2D rigiBody = null;
        #endregion

        #region unity calls
        void Awake()
        {
            rigiBody = GetComponent<Rigidbody2D>();
        }

        void Update()
        {
            rotDiff.z = speedRot * Time.deltaTime;
            rigiBody.MoveRotation(rotDiff * transform.rotation);
        }

        private void FixedUpdate()
        {
            rigiBody.MovePosition(Vector2.zero);
        }
        #endregion
    }
}
