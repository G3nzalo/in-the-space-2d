﻿using UnityEngine;

namespace Juego.Backgrounds
{
    public class fondoNoNivels : MonoBehaviour
    {
        #region vars unity
        [Header("Vars")]
        [SerializeField] private float speedMovHorizontal = 0.0f;
        [SerializeField] private float speedMovVertical = 0.0f;
        [SerializeField] private float movXmin = 0.0f;
        [SerializeField] private float movXmax = 0.0f;
        [SerializeField] private float movYmin = 0.0f;
        [SerializeField] private float movYmax = 0.0f;
        #endregion

        private SpriteRenderer renderer3 = null;

        #region unity calls
        private void Awake()
        {
            renderer3 = GetComponent<SpriteRenderer>();
        }

        private void Update()
        {
            MoverFondoConstante();
        }
        #endregion

        private void MoverFondoConstante()
        {
            if (renderer3.size.x <= movXmax || renderer3.size.y <= movYmax)
            {
                renderer3.size += new Vector2(speedMovHorizontal / 2.0f * Time.deltaTime, speedMovVertical / 2.0f * Time.deltaTime);
            }
            else
            {
                renderer3.size = new Vector2(movXmin, movYmin);
            }
        }

    }
}
