﻿using UnityEngine;

namespace Juego.Backgrounds
{
    public class moverFondo : MonoBehaviour
    {
        #region unity vars
        [Header("Vars")]
        [SerializeField] private float speedMovHorizontal = 0.0f;
        [SerializeField] private float speedMovVertical = 0.0f;
        [SerializeField] private float movXmin = 0.0f;
        [SerializeField] private float movXmax = 0.0f;
        [SerializeField] private float movYmin = 0.0f;
        [SerializeField] private float movYmax = 0.0f;
        #endregion

        private SpriteRenderer renderer3 = null;

        #region unity calls
        private void Awake()
        {
            renderer3 = GetComponent<SpriteRenderer>();
        }

        private void Update()
        {
            ReadAxisMoverFondoHorizontal();
            ReadAxisMoverFondoVertical();
        }
        #endregion

        #region private methods
        private void ReadAxisMoverFondoHorizontal()
        {
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
            {
                if (renderer3.size.x <= movXmax)
                {
                    renderer3.size += new Vector2(speedMovHorizontal * Time.deltaTime, 0.0f);
                }

                else
                {
                    renderer3.size = new Vector2(movXmin, 0.0f);
                }

            }

        }

        private void ReadAxisMoverFondoVertical()
        {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
            {
                if (renderer3.size.y <= movYmax)
                {
                    renderer3.size += new Vector2(0.0f, speedMovVertical * Time.deltaTime);
                }

                else
                {
                    renderer3.size = new Vector2(0.0f, movYmin);
                }

            }

        }
        #endregion

    }
}
